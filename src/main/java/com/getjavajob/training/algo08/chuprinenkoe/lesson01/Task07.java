package com.getjavajob.training.algo08.chuprinenkoe.lesson01;

/**
 * Created by chuprinenkoe on 30.03.16.
 */
public class Task07 {
    public static void main(String[] args) {
        System.out.println(swapValuesOfTwoVariables1(2, 3));
        System.out.println(swapValuesOfTwoVariables2(2, 3));
        System.out.println(swapValuesOfTwoVariables3(12, 13));
        System.out.println(swapValuesOfTwoVariables4(12, 13));
    }

    public static String swapValuesOfTwoVariables1(int a, int b) {
        a += b;
        b = a - b;
        a -= b;
        return "a = " + a + ", b = " + b;
    }

    public static String swapValuesOfTwoVariables2(int a, int b) {
        a *= b;
        b = a / b;
        a /= b;
        return "a = " + a + ", b = " + b;
    }

    public static String swapValuesOfTwoVariables3(int a, int b) {
        a ^= b;
        b ^= a;
        a ^= b;
        return "a = " + a + ", b = " + b;
    }

    public static String swapValuesOfTwoVariables4(int a, int b) {
        a = ~a ^ ~b;
        b ^= a;
        a ^= b;
        return "a = " + a + ", b = " + b;
    }
}
