package com.getjavajob.training.algo08.chuprinenkoe.lesson01;
import static com.getjavajob.training.algo08.chuprinenkoe.lesson01.Task07.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;
/**
 * Created by chuprinenkoe on 30.03.16.
 */
public class Task07Test {
    public static void main(String[] args) {
        testSwapValuesOfTwoVariables1();
        testSwapValuesOfTwoVariables2();
        testSwapValuesOfTwoVariables3();
        testSwapValuesOfTwoVariables4();
    }

    public static void testSwapValuesOfTwoVariables1() {
        assertEquals("Task07Test.testSwapValuesOfTwoVariables1", "a = 3, b = 2", swapValuesOfTwoVariables1(2, 3));
    }

    public static void testSwapValuesOfTwoVariables2() {
        assertEquals("Task07Test.testSwapValuesOfTwoVariables2", "a = 3, b = 2", swapValuesOfTwoVariables1(2, 3));
    }

    public static void testSwapValuesOfTwoVariables3() {
        assertEquals("Task07Test.testSwapValuesOfTwoVariables3", "a = 3, b = 2", swapValuesOfTwoVariables1(2, 3));
    }

    public static void testSwapValuesOfTwoVariables4() {
        assertEquals("Task07Test.testSwapValuesOfTwoVariables3", "a = 3, b = 2", swapValuesOfTwoVariables1(2, 3));
    }


}
