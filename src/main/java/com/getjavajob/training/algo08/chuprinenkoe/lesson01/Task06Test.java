package com.getjavajob.training.algo08.chuprinenkoe.lesson01;

import static com.getjavajob.training.algo08.chuprinenkoe.lesson01.Task06.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 30.03.16.
 */
public class Task06Test {
    public static void main(String[] args) {
        testCalculateExpressionA();
        test1CalculateExpressionB();
        test2CalculateExpressionB();
        testResetNLowerBits();
        test1SetNBitWith1();
        test2SetNBitWith1();
        test1invertNBit();
        test2invertNBit();
        test1SetNBitWith0();
        test2SetNBitWith0();
        testReturnNLowerBits();
        test1ReturnNBit();
        test2ReturnNBit();
        testCreateBinRepresentationOfNumber();
    }

    public static void testCalculateExpressionA() {
        assertEquals("Task06Test.testCalculateExpressionA", 0b1000, calculateExpressionA(3));
    }

    public static void test1CalculateExpressionB() {
        assertEquals("Task06Test.test1CalculateExpressionB", 0b1000, calculateExpressionB(2, 2));
    }

    public static void test2CalculateExpressionB() {
        assertEquals("Task06Test.test2CalculateExpressionB", 0b1100, calculateExpressionB(2, 3));
    }

    public static void testResetNLowerBits() {
        assertEquals("Task06Test.testResetNLowerBits", 0b11000, resetNLowerBits(0b11111, 3));
    }

    public static void test1SetNBitWith1() {
        assertEquals("Task06Test.test1SetNBitWith1", 0b10001, setNBitWith1(0b10000, 0));
    }

    public static void test2SetNBitWith1() {
        assertEquals("Task06Test.test2SetNBitWith1", 0b10001, setNBitWith1(0b10001, 0));
    }

    public static void test1invertNBit() {
        assertEquals("Task06Test.test1InvertNBit", 0b1000, invertNBit(0b1010, 1));
    }

    public static void test2invertNBit() {
        assertEquals("Task06Test.test2InvertNBit", 0b1010, invertNBit(0b1000, 1));
    }

    public static void test1SetNBitWith0() {
        assertEquals("Task06Test.test1SetNBitWith0", 0b11011, setNBitWith0(0b11111, 2));
    }

    public static void test2SetNBitWith0() {
        assertEquals("Task06Test.test2SetNBitWith0", 0b10000, setNBitWith0(0b10000, 2));
    }

    public static void testReturnNLowerBits() {
        assertEquals("Task06Test.testReturnNLowerBits", 0b111, returnNLowerBits(0b11111, 3));
    }

    public static void test1ReturnNBit() {
        assertEquals("Task06Test.test1ReturnNBit", 1, returnNBit(0b10101, 2));
    }

    public static void test2ReturnNBit() {
        assertEquals("Task06Test.test2ReturnNBit", 0, returnNBit(0b10101, 3));
    }

    public static void testCreateBinRepresentationOfNumber() {
        assertEquals("Task06Test.testCreateBinRepresentationOfNumber", "00001111",
                createBinRepresentationOfNumber((byte) 15));
    }
}
