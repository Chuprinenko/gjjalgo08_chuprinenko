package com.getjavajob.training.algo08.chuprinenkoe.lesson01;

/**
 * Created by chuprinenkoe on 28.03.16.
 */
public class Task06 {
    public static void main(String[] args) {
        System.out.println(calculateExpressionA(4));
        System.out.println(calculateExpressionB(3, 3));
        System.out.println(resetNLowerBits(15, 3));
        System.out.println(setNBitWith1(4, 0));
        System.out.println(invertNBit(6, 1));
        System.out.println(setNBitWith0(7, 1));
        System.out.println(returnNLowerBits(10, 4));
        System.out.println(returnNBit(8, 1));
        System.out.println(createBinRepresentationOfNumber((byte) 15));
    }

    /**
     * This method raises the number 2 to the power of n
     *
     * @param n is the degree (n < 31)
     * @return value type int
     */
    public static int calculateExpressionA(int n) {
        return 1 << n;
    }

    /**
     * This method calculate expression 2^n + 2^m
     *
     * @param n is the degree (n < 31)
     * @param m is the degree (n < 31)
     * @return value type int
     */
    public static int calculateExpressionB(int n, int m) {
        return n == m ? 1 << n << 1 : 1 << n ^ 1 << m;
    }

    /**
     * This method resets lower bits in number
     *
     * @param number the value is passed to the method
     * @param n      the quantity of lower bits reset
     * @return value type int
     */
    public static int resetNLowerBits(int number, int n) {
        return number >> n << n;
    }

    /**
     * This method set n-th bit with 1 in number
     *
     * @param number the value is passed to the method
     * @param n      the number of bits which is set to 1
     * @return value type int
     */
    public static int setNBitWith1(int number, int n) {
        return number | 1 << n;
    }

    /**
     * This method invert n-th bit in number
     *
     * @param number the value is passed to the method
     * @param n      the bit number to be inverted
     * @return value type int
     */
    public static int invertNBit(int number, int n) {
        return number ^ 1 << n;
    }

    /**
     * his method set n-th bit with 0 in number
     *
     * @param number the value is passed to the method
     * @param n      the number of bits which is set to 0
     * @return value type int
     */
    public static int setNBitWith0(int number, int n) {
        return number == (number | 1 << n) ? number ^ 1 << n : number;
    }

    /**
     * This method returns lower bits from number
     *
     * @param number the value is passed to the method
     * @param n      the quantity of lower bits to return
     * @return value type int
     */
    public static int returnNLowerBits(int number, int n) {
        return ~(~0 << n) & number;
    }

    /**
     * The method returns the value of n-th bit
     *
     * @param number the value is passed to the method
     * @param n      the number of bit
     * @return value type int
     */
    public static int returnNBit(int number, int n) {
        return number == (number | 1 << n) ? 1 : 0;
    }

    /**
     * This method create bin representation of number
     *
     * @param number the value is passed to the method
     * @return object of class String
     */
    public static String createBinRepresentationOfNumber(byte number) {
        StringBuilder bin = new StringBuilder();
        for (int i = 7; i >= 0; i--) {
            bin.append(number == (number | 1 << i) ? 1 : 0);
        }
        return bin.toString();
    }
}
