package com.getjavajob.training.algo08.util;

import java.util.Arrays;

/**
 * Created by chuprinenkoe on 30.03.16.
 */
public class Assert {
    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected[][], double actual[][]) {
        if (Arrays.deepEquals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.deepToString(expected) +
                    ", actual " + Arrays.deepToString(actual));
        }
    }

    public static void assertEquals(String testName, int expected[][], int actual[][]) {
        if (Arrays.deepEquals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.deepToString(expected) + ", actual " +
                    Arrays.deepToString(actual));
        }
    }

    public static void assertEquals(String testName, int expected[], int actual[]) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual " +
                    Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, double expected[], double actual[]) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual " +
                    Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, char expected, char actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }
}
